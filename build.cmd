@ECHO OFF
setlocal EnableDelayedExpansion

set "mvn_command=mvn clean package -DskipTests=true"

@ECHO ON

@REM execute mvn command
@ECHO "Executing mvn command"
%mvn_command%

@REM check if mvn command was successful
if %ERRORLEVEL% NEQ 0 (
    @ECHO "Maven command failed"
    exit /B 1
)

@REM copy jar file to src/main/docker
@ECHO "Copying jar file to src/main/docker"
copy target/*.jar src/main/docker