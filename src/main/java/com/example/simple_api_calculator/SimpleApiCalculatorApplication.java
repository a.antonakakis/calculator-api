package com.example.simple_api_calculator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EntityScan("com.example.simple_api_calculator")
@EnableJpaRepositories("com.example.simple_api_calculator")
public class SimpleApiCalculatorApplication {

	public static void main(String[] args) {
		SpringApplication.run(SimpleApiCalculatorApplication.class, args);
	}

}
