package com.example.simple_api_calculator;

import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.provisioning.JdbcUserDetailsManager;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.security.web.csrf.*;
import org.springframework.util.StringUtils;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.sql.DataSource;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.function.Supplier;

@EnableWebSecurity
@Configuration
public class SecurityConfiguration {
    private static final String CSRF_HEADER = "X-calc-csrf";
    private static final String CSRF_COOKIE_NAME = "XSRF-TOKEN";


    @Autowired
    private DataSource dataSource;

    @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {

        http.cors(securityBuilder -> securityBuilder.configurationSource(corsConfigurationSource()))
                .csrf(securityBuilder -> securityBuilder//.csrfTokenRepository(httpSessionCsrfTokenRepository())
                        .csrfTokenRepository(CookieCsrfTokenRepository.withHttpOnlyFalse())
                        .csrfTokenRequestHandler(new GoBeyondCsrfTokenRequestHandler()))
                //.addFilterAfter(new CsrfCookieFilter(), BasicAuthenticationFilter.class)
                .authorizeHttpRequests((requests) -> requests.requestMatchers("/*")
                        .permitAll()
                        .anyRequest()
                        .authenticated())
                .httpBasic(Customizer.withDefaults())
                //.httpBasic(t -> t.securityContextRepository(null).securityConfigurerAdapter())
                /*.formLogin((form) -> form
                    .loginPage("/login")
                    .permitAll()
                )*/
                .logout((logout) -> logout.permitAll().clearAuthentication(true));

        return http.build();

    }


/*
    // Since we are using a database to store users, we don't need this
    @Bean
    public InMemoryUserDetailsManager userDetailsService() {
        UserDetails user = User.withDefaultPasswordEncoder()
                .username("dev")
                .password("1234")
                .roles("USER")
                .build();
        return new InMemoryUserDetailsManager(user);
    }
 */

    @Bean
    UserDetailsManager users() {
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();

        // create two users


        UserDetails user = User.builder()
                .username("user")
                .password(encoder.encode("1234"))
                .roles("USER")
                .build();


        UserDetails admin = User.builder()
                .username("admin")
                .password(encoder.encode("12345"))
                .roles("USER", "ADMIN")
                .build();
        JdbcUserDetailsManager users = new JdbcUserDetailsManager(dataSource);

        // for debug purposes, we delete the users and recreate them each time we deploy
        users.deleteUser("user");
        users.deleteUser("admin");

        users.createUser(user);
        users.createUser(admin);

        return users;
    }

    // we use bcrypt to encrypt the password, hence we need the encoder to encrypt the provided password
    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    private CorsConfigurationSource corsConfigurationSource() {

        CorsConfiguration configuration = new CorsConfiguration();
        configuration.setAllowedOrigins(List.of("http://localhost:8080/", "http://localhost:8080/api/calc/getall/", "http://192.168.133.10:8080/"));
        configuration.setAllowedMethods(List.of("GET","POST", "OPTIONS"));
        configuration.setAllowCredentials(true);
        configuration.setAllowedHeaders(List.of("Authorization", CSRF_HEADER, "Access-Control-Request-Headers", "Access-Control-Allow-Origin"));

        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();

        source.registerCorsConfiguration("/**", configuration);

        return  source;
    }

    private HttpSessionCsrfTokenRepository httpSessionCsrfTokenRepository() {
        var repository = new HttpSessionCsrfTokenRepository();
        repository.setHeaderName(CSRF_HEADER);
        return repository;
    }


    static final class CsrfCookieFilter extends OncePerRequestFilter {

        @Override
        protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
                throws ServletException, IOException {
            CsrfToken csrfToken = (CsrfToken) request.getAttribute("_csrf");
            // Render the token value to a cookie by causing the deferred token to be loaded
            csrfToken.getToken();

            response.addCookie(new Cookie(CSRF_COOKIE_NAME ,csrfToken.getToken()));
            //response.setHeader(CSRF_HEADER, csrfToken.getToken());

            filterChain.doFilter(request, response);
        }

    }

    private final class GoBeyondCsrfTokenRequestHandler extends CsrfTokenRequestAttributeHandler {
        private CsrfTokenRequestHandler handler = new XorCsrfTokenRequestAttributeHandler();
        //private static final Logger log = LoggerFactory.getLogger(GoBeyondCsrfTokenRequestHandler.class);


        @Override
        public void handle(HttpServletRequest request, HttpServletResponse response, Supplier<CsrfToken> csrfToken) {
            /*
             * Always use XorCsrfTokenRequestAttributeHandler to provide BREACH protection of
             * the CsrfToken when it is rendered in the response body.
             */
            this.handler.handle(request, response, csrfToken);
        }

        @Override
        public String resolveCsrfTokenValue(HttpServletRequest request, CsrfToken csrfToken) {
            /*
             * If the request contains a request header, use CsrfTokenRequestAttributeHandler
             * to resolve the CsrfToken. This applies when a single-page application includes
             * the header value automatically, which was obtained via a cookie containing the
             * raw CsrfToken.
             */
            if (StringUtils.hasText(request.getHeader(csrfToken.getHeaderName()))) {
                //log.info("INSIDE RESOLVE 1: " + csrfToken.getToken());
                return super.resolveCsrfTokenValue(request, csrfToken);
            }
            //Optional<Cookie> csrfOnCookie = Optional.ofNullable(new Cookie("",""));
            Optional<Cookie> cookie = Arrays.asList(request.getCookies()).stream()
                    //.map(t -> {
                    //log.info("Cookie: name: " + t.getName() + " value: " + t.getValue());
                    //return t;
                    //})
                    .filter(t -> t.getName().equals(CSRF_COOKIE_NAME)).findAny();
            if (cookie.isPresent() && cookie.get().getValue().equals(csrfToken.getToken())) {
                //log.info("INSIDE RESOLVE 2: " + csrfToken.getToken() );
                return csrfToken.getToken();//super.resolveCsrfTokenValue(request, csrfToken);
            }


            /*
             * In all other cases (e.g. if the request contains a request parameter), use
             * XorCsrfTokenRequestAttributeHandler to resolve the CsrfToken. This applies
             * when a server-side rendered form includes the _csrf request parameter as a
             * hidden input.
             */
            return this.handler.resolveCsrfTokenValue(request, csrfToken);
        }
    }

}



