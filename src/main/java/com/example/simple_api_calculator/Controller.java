package com.example.simple_api_calculator;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/calc")
@RequiredArgsConstructor
@CrossOrigin(origins = "*", allowedHeaders = "*")
@Slf4j
public class Controller {
@Autowired
private CalcService calcService;

@PostMapping(value = "/add", consumes = "application/json")
public Calculation add(@RequestBody Calculation calculation) {
    log.info("add: {} + {}", calculation.getNum1(), calculation.getNum2());
    return calcService.add(calculation);
}

@PostMapping(value = "/sub", consumes = "application/json")
public Calculation subtract(@RequestBody Calculation calculation) {
    log.info("subtract: {} - {}", calculation.getNum1(), calculation.getNum2());
    return calcService.subtract(calculation);
}

@PostMapping(value = "/mul", consumes = "application/json")
public Calculation multiply(@RequestBody Calculation calculation) {
    log.info("multiply: {} * {}", calculation.getNum1(), calculation.getNum2());
    return calcService.multiply(calculation);
}

@PostMapping(value = "/div", consumes = "application/json")
public Calculation divide(@RequestBody Calculation calculation) {
    log.info("divide: {} / {}", calculation.getNum1(), calculation.getNum2());
    return calcService.divide(calculation);
}
@PostMapping(value = "/read/{id}", produces = "application/json")
public Calculation read(@PathVariable int id) {
    log.info("read: {}", id);
    return calcService.read(id);
}

@PostMapping(value = "/getall", produces = "application/json")
public Iterable<Calculation> getAll() {
    log.info("getAll");
    return calcService.getAll();
}

@PostMapping(value = "/update", consumes = "application/json")
public Calculation update(@RequestBody Calculation calculation) {
    log.info("update: {}", calculation.getId());
    return calcService.update(calculation);
}

@GetMapping(value = "/delete/{id}")
public void delete(@PathVariable int id) {
    log.info("delete: {}", id);
    calcService.delete(id);
}

@GetMapping(value = "/clear")
public void clear() {
    log.info("clear");
    calcService.clear();
}

}
