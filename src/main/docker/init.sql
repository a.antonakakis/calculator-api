CREATE DATABASE calculator;
\c calculator;
CREATE TABLE users( username VARCHAR(50) NOT null PRIMARY KEY, password VARCHAR(500) NOT null, enabled BOOLEAN NOT null );
CREATE TABLE authorities (username VARCHAR(50) NOT null,authority VARCHAR(50) NOT null,CONSTRAINT fk_authorities_users FOREIGN KEY(username) REFERENCES users(username) );
CREATE UNIQUE INDEX ix_auth_username ON authorities (username,authority);
CREATE TABLE calculations(id SERIAL PRIMARY KEY, num1 REAL, op VARCHAR(255) ,num2 REAL, res REAL);
